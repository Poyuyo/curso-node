// funcion con nombre o nombrada
function suma(x: number , y: number):number {
    return x + y
}
// funcion flecha
const sumaFlecha = (x: number , y: number):number => {
    return x + y
}
// console.log(suma(1,2) );

// funcion con parametros opcionales
const funcionOpcionalSuma = (x:number, y?:number):number =>{
    if(!y) return x 
    return x + y
}
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));

// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x:number, y:number = 0):number =>{
    return x + y
}
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1,2));


const functionOptionalProduct = (x:number, y:number = 1):number =>{
    return x*y;
}

console.log("functionOptionalProduct(5, 3) = "+functionOptionalProduct(5, 3));
console.log("functionOptionalProduct(5) = " +functionOptionalProduct(5));

const functionOptionalDiv = (x:number, y:number = 1):number =>{
    if(y!=0)
        return x/y;
    else
        return x;
}

console.log("functionOptionalDiv(20/5) = " + functionOptionalDiv(20,5))
console.log("functionOptionalDiv(20/0) = " + functionOptionalDiv(20,0))
console.log("functionOptionalDiv(20) = " + functionOptionalDiv(20))